package me.steven.networkreward.playerfile;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import me.steven.networkreward.NetworkReward;
import me.steven.networkreward.objectives.Objective;
import me.steven.networkreward.objectives.Type;

public class PlayerFileManager {
	private NetworkReward plugin;
	private List<PlayerFile> playerFile = new ArrayList<>();

	public PlayerFileManager(NetworkReward plugin) {
		this.plugin = plugin;
	}

	public void init() {
		// TODO load players when join and unload when leave.
		loadPlayerFiles();
	}

	private void loadPlayerFiles() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			loadPlayerFile(p);
		}
	}

	public PlayerFile getPlayerFile(OfflinePlayer p) {
		PlayerFile pf = playerFile.stream().filter(d -> d.getP() == p).findFirst().orElse(null);
		return pf;
	}

	private boolean fileExistCheck(File file, Player p) {
		try {
			File dir = new File(plugin.getDataFolder().getPath() + "/players");
			if (!dir.exists()) {
				dir.mkdirs();
			}
			if (!file.exists()) {
				plugin.getLogger().info(p.getUniqueId().toString() + ".yml not found, creating!");
				file.createNewFile();
				// create default player
				Objective randomObjective = plugin.getObjectiveManager().getRandomObjective();
				FileConfiguration da = YamlConfiguration.loadConfiguration(file);
				int amountDone = da.getInt(".amountDone");
				PlayerFile de = new PlayerFile(p, randomObjective, plugin, amountDone);
				Random randomGenerator = new Random();
				de.setBlock(Material.DIAMOND_BLOCK);
				de.setMob(EntityType.WITHER);
				de.setRequirements(new ArrayList<String>());
				if (randomObjective.getType().equals(Type.BUILD) || randomObjective.getType().equals(Type.DESTROY)) {
					int blockNum = randomGenerator.nextInt(randomObjective.getBlocks().size());
					Material newBlock = randomObjective.getBlocks().get(blockNum);
					de.setBlock(newBlock);
				}
				if (randomObjective.getType().equals(Type.KILL)) {
					int mobNum = randomGenerator.nextInt(randomObjective.getMobs().size());
					EntityType mob = randomObjective.getMobs().get(mobNum);
					de.setMob(mob);
				}
				de.save();
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return true;
	}

	private PlayerFile getPlayer(FileConfiguration da, Player p) {
		Objective obj = plugin.getObjectiveManager().getObjective(da.getString(".objective"));
		PlayerFile de = new PlayerFile(p, obj, plugin, da.getInt(".amountDone"));
		if (da.getString(".block") != null) {
			de.setBlock(Material.valueOf(da.getString(".block")));
		}
		if (da.getString(".mob") != null) {
			de.setMob(EntityType.valueOf(da.getString(".mob")));
		}
		if(da.getStringList(".require") != null){
			de.setRequirements(da.getStringList(".require"));
		}else{
			de.setRequirements(new ArrayList<String>());
		}
		if(da.getString(".scoreboard") != null){
			de.setScoreboardEnabled(da.getBoolean(".scoreboard"));
		}
		return de;

	}

	public boolean loadPlayerFile(Player p) {
		File file = new File(plugin.getDataFolder() + "/players", p.getUniqueId().toString() + ".yml");
		fileExistCheck(file, p);
		FileConfiguration da = YamlConfiguration.loadConfiguration(file);
		PlayerFile pf = getPlayer(da, p);
		playerFile.add(pf);
		return true;
	}

	public void unloadPlayerFile(Player p) {
		getPlayerFile(p).save();
		PlayerFile pf = getPlayerFile(p);
		List<PlayerFile> li = playerFile;
		li.remove(pf);
		playerFile = li;
	}

}
