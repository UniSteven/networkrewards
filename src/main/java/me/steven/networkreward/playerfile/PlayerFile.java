package me.steven.networkreward.playerfile;

import me.steven.networkreward.NetworkReward;
import me.steven.networkreward.mysql.PlayerData;
import me.steven.networkreward.objectives.Objective;
import me.steven.networkreward.objectives.Type;
import me.steven.networkreward.rewards.Reward;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Random;

public class PlayerFile {
    private Player p;
    private Objective objective;
    private int amountDone;
    private NetworkReward plugin;
    private int coins = 0;
    private Material block;
    private EntityType mob;
    private boolean updatedValue = true;
    private int menuNum = 1;
    private int playTime = -1;
    private List<String> requirements;
    private boolean scoreboardEnabled = true;

    public PlayerFile(Player p, Objective objective, NetworkReward plugin, int amountDone) {
        this.p = p;
        this.objective = objective;
        this.plugin = plugin;
        this.amountDone = amountDone;
    }

    public String getConvertedPlayTime() {
        getPlayTime();
        String result = "";
        if(playTime < 60){
            result = playTime + " mins";
        }
        if(playTime > 60 && playTime < (24*60)){
            result = (playTime / 60) + " hours " + (playTime % 60) + " mins";
        }
        if(playTime > (60*24)){
            result = ((playTime / 60) / 24) + "Days " + ((playTime  /60) % 24)  + " hours " + (playTime % 60) + " mins";
        }
        return result;
    }

    public int getPlayTime() {
        if (playTime == -1) {
            PlayerData playerData = new PlayerData(plugin);
            playTime = playerData.getTime(p.getUniqueId());
        }
        return playTime;
    }

    public void setPlayTime(int playTime) {
        this.playTime = playTime;
    }

    public Player getP() {
        return p;
    }

    public void setP(Player p) {
        this.p = p;
    }

    public Objective getObjective() {
        return objective;
    }

    public void setObjective(Objective objective) {
        this.objective = objective;
    }

    public int getAmountDone() {
        return amountDone;
    }

    public void setAmountDone(int amountDone) {
        this.amountDone = amountDone;
        checkDone();
    }

    public void incrementAmountDone() {
        this.amountDone++;
        checkDone();
    }

    public int getCoins() {
        // this.coins = plugin.getPlayerData().getCoins(p.getUniqueId());
        return coins;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }

    public boolean isScoreboardEnabled() {
        return scoreboardEnabled;
    }

    public void setScoreboardEnabled(boolean scoreboardEnabled) {
        this.scoreboardEnabled = scoreboardEnabled;
    }

    private void checkDone() {
        int target = objective.getAmount();
        if (amountDone >= target) {
            // give reward.
            Reward reward = objective.getReward();
            int coins = reward.getCoins();
            if (coins != 0) {
                plugin.getPlayerData().addCoins(p.getUniqueId(), coins);
            }
            String message = ChatColor.translateAlternateColorCodes('&',
                    reward.getMessage().replace("{prefix}", plugin.getConfig().getString(".prefix")));
            if (message != null) {
                p.sendMessage(message);
            }
            if (reward.getCommand() != null) {
                Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
                        reward.getCommand().replace("{player}", p.getName()));

            }
            objective = plugin.getObjectiveManager().getRandomObjective();
            Random randomGenerator = new Random();
            if (objective.getType().equals(Type.BUILD) || objective.getType().equals(Type.DESTROY)) {
                int blockNum = randomGenerator.nextInt(objective.getBlocks().size());
                Material newBlock = objective.getBlocks().get(blockNum);
                plugin.getPlayerFileManager().getPlayerFile(p).setBlock(newBlock);
            }
            if (objective.getType().equals(Type.KILL)) {
                int mobNum = randomGenerator.nextInt(objective.getMobs().size());
                EntityType mob = objective.getMobs().get(mobNum);
                plugin.getPlayerFileManager().getPlayerFile(p).setMob(mob);

            }

            amountDone = 0;
        }
    }

    public Material getBlock() {
        return block;
    }

    public void setBlock(Material block) {
        this.block = block;
    }

    public EntityType getMob() {
        return mob;
    }

    public void setMob(EntityType mob) {
        this.mob = mob;
    }

    public boolean isUpdatedValue() {
        return updatedValue;
    }

    public void setUpdatedValue(boolean updatedValue) {
        this.updatedValue = updatedValue;
    }

    public int getMenuNum() {
        return menuNum;
    }

    public void setMenuNum(int menuNum) {
        this.menuNum = menuNum;
    }

    public List<String> getRequirements() {
        return requirements;
    }

    public void setRequirements(List<String> requirements) {
        this.requirements = requirements;
    }

    public void addRequirement(String requirement) {
        if (!requirements.contains(requirement)) {
            requirements.add(requirement);
        }
    }

    public boolean hasRequirement(String s) {
        return requirements.contains(s);
    }

    public void save() {
        File file = new File(plugin.getDataFolder() + "/players", p.getUniqueId().toString() + ".yml");
        FileConfiguration da = YamlConfiguration.loadConfiguration(file);
        da.set(".objective", objective.getName());
        da.set(".amountDone", amountDone);
        da.set(".block", block.toString());
        da.set(".mob", mob.toString());
        da.set(".require", requirements);
        da.set(".scoreboard", scoreboardEnabled);
        try {
            da.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
