package me.steven.networkreward.mysql;

import me.steven.networkreward.NetworkReward;
import me.steven.networkreward.playerfile.PlayerFile;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class PlayerData {
    private NetworkReward plugin;
    private static final int DEFAULT_COIN_AMOUNT = 0;
    // private boolean valuesUpdated = true;

    public PlayerData(NetworkReward plugin) {
        this.plugin = plugin;
    }

    public int getCoins(UUID uuid) {
        Player p = Bukkit.getPlayer(uuid);
        PlayerFile pf = null;
        if (p != null) {
            pf = plugin.getPlayerFileManager().getPlayerFile(p);
        }
        if (pf != null && !pf.isUpdatedValue()) {
            int coins = pf.getCoins();
            return coins == 0 ? DEFAULT_COIN_AMOUNT : coins;
        }
        try {
            PreparedStatement ps = plugin.getConnection().getConnection()
                    .prepareStatement("SELECT Coins FROM playerdata WHERE Uuid=?");
            ps.setString(1, uuid.toString());
            ResultSet rs = ps.executeQuery();
            int coins = rs.next() ? rs.getInt("Coins") : DEFAULT_COIN_AMOUNT;
            ps.close();
            if (pf != null) {
                plugin.getPlayerFileManager().getPlayerFile(p).setCoins(coins);
                pf.setUpdatedValue(false);
            }
            if (p != null) {
                plugin.getLogger().info("Debug: recieved playerdata for: " + p.getName());
            }
            return coins;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    public void addCoins(UUID uuid, int coins) {
        setCoins(uuid, coins + getCoins(uuid));
    }

    private void setCoins(UUID uuid, int coins) {
        Player p = Bukkit.getPlayer(uuid);
        PlayerFile pf = null;
        if (p != null) {
            pf = plugin.getPlayerFileManager().getPlayerFile(p);
        }
        try {
            PreparedStatement ps = plugin.getConnection().getConnection().prepareStatement(
                    "INSERT INTO playerdata (UUID, Coins) VALUES (?, ?) ON DUPLICATE KEY UPDATE Coins=?");
            ps.setString(1, uuid.toString());
            ps.setInt(2, coins);
            ps.setInt(3, coins);
            ps.executeUpdate();
            ps.close();
            if (pf != null) {
                pf.setUpdatedValue(true);
                pf.setCoins(coins);
            }
            if(p !=null) {
                plugin.getLogger().info("Debug: set playerdata for: " + p.getName());
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void setTime(UUID uuid, int time) {
        try {
            PreparedStatement ps = plugin.getConnection().getConnection().prepareStatement(
                    "INSERT INTO playertime (UUID, Time) VALUES (?, ?) ON DUPLICATE KEY UPDATE Time=?");
            ps.setString(1, uuid.toString());
            ps.setInt(2, time);
            ps.setInt(3, time);
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public int getTime(UUID uuid) {
        try {
            PreparedStatement ps = plugin.getConnection().getConnection()
                    .prepareStatement("SELECT Time FROM playertime WHERE Uuid=?");
            ps.setString(1, uuid.toString());
            ResultSet rs = ps.executeQuery();
            int coins = rs.next() ? rs.getInt("Time") : 0;
            ps.close();
            return coins;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void incrementTimePlayer(UUID uuid) {
        setTime(uuid, (getTime(uuid) + 1));
    }

}
