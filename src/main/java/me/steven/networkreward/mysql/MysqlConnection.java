package me.steven.networkreward.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.bukkit.configuration.file.FileConfiguration;

import me.steven.networkreward.NetworkReward;

public class MysqlConnection {
	private NetworkReward plugin;
	private FileConfiguration config;
	private String host;
	private int port;
	private String database;
	private String username;
	private String password;
	private Connection connection;

	public MysqlConnection(NetworkReward plugin) {
		this.plugin = plugin;
		config = plugin.getConfig();
		host = config.getString("mysql.host");
		port = config.getInt("mysql.port");
		database = config.getString("mysql.database");
		username = config.getString("mysql.user");
		password = config.getString("mysql.password");

	}

	public Connection getConnection() {
		return connection;
	}

	public boolean init() {
		try {
			openConnection();
			plugin.getLogger().info("Connection established with the database!");
			// String search = "test";
			PreparedStatement ps = getConnection().prepareStatement(
					"CREATE TABLE IF NOT EXISTS PlayerData ( `UUID` VARCHAR(255) NOT NULL , `Coins` INT(255) NOT NULL DEFAULT '0' , PRIMARY KEY (`UUID`)) ENGINE = InnoDB;");
			PreparedStatement ps1 = getConnection().prepareStatement(
					"CREATE TABLE IF NOT EXISTS PlayerTime ( `UUID` VARCHAR(255) NOT NULL , `Time` INT(255) NOT NULL DEFAULT '0' , PRIMARY KEY (`UUID`)) ENGINE = InnoDB;");
			// ps.setString(1, search); // vervangt eerste vraagteken met de
			// content.
			ps.executeUpdate();
			ps.close();
			ps1.executeUpdate();
			ps1.close();
			return true;
		} catch (ClassNotFoundException e) {
			// e.printStackTrace();
			return false;
		} catch (SQLException e) {
			// e.printStackTrace();
			plugin.getLogger().info("Connection failed with the database! Details correct?");
			return false;
		}
	}

	// open connect
	private void openConnection() throws SQLException, ClassNotFoundException {
		if (connection != null && !connection.isClosed()) {
			return;
		}

		synchronized (this) {
			if (connection != null && !connection.isClosed()) {
				return;
			}
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(
					"jdbc:mysql://" + this.host + ":" + this.port + "/" + this.database + "?autoReconnect=true",
					this.username, this.password);
		}
	}
}
