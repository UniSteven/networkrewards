package me.steven.networkreward.listener;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import me.steven.networkreward.NetworkReward;
import me.steven.networkreward.playerfile.PlayerFile;
import me.steven.networkreward.shop.ShopItem;
import net.md_5.bungee.api.ChatColor;

public class InventoryClick implements Listener {

	private NetworkReward plugin;

	public InventoryClick(NetworkReward plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {

		if (e.getCurrentItem() == null || (e.getCurrentItem().getType() == Material.AIR)) {
			return;
		}
		Player p = (Player) e.getWhoClicked();
		String inventoryName = e.getInventory().getName();
		PlayerFile pf = plugin.getPlayerFileManager().getPlayerFile(p);
		int menuNum = pf.getMenuNum();
		String playerMenuName = ChatColor.translateAlternateColorCodes('&',
				plugin.getConfig().getString(".shopName") + menuNum);
		if (playerMenuName.equals(inventoryName)) {
			int slotNum = e.getSlot();
			List<ShopItem> items = plugin.getShopItemManager().getShopItems();
			ShopItem item = items.get((slotNum * menuNum));
			//check requirements
			if(!item.getRequirement().equals("")){
			if(!pf.hasRequirement(item.getRequirement().toLowerCase())){
				String message = ChatColor.translateAlternateColorCodes('&',
						plugin.getConfig().getString(".noRequire").replace("{prefix}", plugin.getConfig().getString(".prefix")).replace("{item}", item.getRequirement()));
				p.sendMessage(message);
				e.setCancelled(true);
				return;
			}
			}
			// check if enough coin
			int cost = item.getCost();
			int coins = pf.getCoins();
			if (coins > cost) {
				plugin.getPlayerData().addCoins(p.getUniqueId(), (cost * -1));
				if (item.getReward().getCoins() != 0) {
					plugin.getPlayerData().addCoins(p.getUniqueId(), item.getReward().getCoins());
				}
				String message = ChatColor.translateAlternateColorCodes('&',
						item.getReward().getMessage().replace("{prefix}", plugin.getConfig().getString(".prefix")));
				if (message != null) {
					p.sendMessage(message);
				}
				if (item.getReward().getCommand() != null) {
					Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
							item.getReward().getCommand().replace("{player}", p.getName()));
				}
				p.closeInventory();
				
				//add to requirement list
				pf.addRequirement(item.getName().toLowerCase());
			} else {
				String message = ChatColor.translateAlternateColorCodes('&', plugin.getConfig()
						.getString(".notEnoughCoins").replace("{prefix}", plugin.getConfig().getString(".prefix")));
				if (message != null) {
					p.sendMessage(message);
				}
			}
			// give player reward.
			e.setCancelled(true);

		}
	}

}
