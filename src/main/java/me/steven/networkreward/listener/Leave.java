package me.steven.networkreward.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import me.steven.networkreward.NetworkReward;

public class Leave implements Listener{
	private NetworkReward plugin;
	
	public Leave(NetworkReward plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onLeave(PlayerQuitEvent e){
		plugin.getPlayerFileManager().getPlayerFile(e.getPlayer()).save();
		plugin.getPlayerFileManager().unloadPlayerFile(e.getPlayer());
	}

}
