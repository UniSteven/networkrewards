package me.steven.networkreward.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import me.steven.networkreward.NetworkReward;
import me.steven.networkreward.playerfile.PlayerFile;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

public class Join implements Listener {

	NetworkReward plugin;

	public Join(NetworkReward plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		// let player get scoreboard is scoreboard is set to enabled.
		BukkitTask run = new BukkitRunnable() {
			@Override
			public void run() {
				plugin.getPlayerFileManager().loadPlayerFile(e.getPlayer());
				PlayerFile pf = plugin.getPlayerFileManager().getPlayerFile(e.getPlayer());
				pf.setCoins(plugin.getPlayerData().getCoins(e.getPlayer().getUniqueId()));
			}
		}.runTaskAsynchronously(plugin);
        if (plugin.getConfig().getBoolean("scoreboard")) {
            plugin.getScoreboardManager().loadPlayer(e.getPlayer());
        }
	}

}
