package me.steven.networkreward.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

import me.steven.networkreward.NetworkReward;
import me.steven.networkreward.objectives.Type;
import me.steven.networkreward.playerfile.PlayerFile;

public class Kill implements Listener {
	NetworkReward plugin;

	public Kill(NetworkReward plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void killEntity(EntityDeathEvent e) {
		if (e.getEntity().getKiller() == null) {
			return;
		}
		Player killer = e.getEntity().getKiller();
		PlayerFile pf = plugin.getPlayerFileManager().getPlayerFile(killer);
		if (pf.getObjective().getType() == Type.KILL) {
			if (e.getEntityType().equals(pf.getMob())) {
				pf.incrementAmountDone();
			}
		}
	}

}
