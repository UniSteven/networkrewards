package me.steven.networkreward.listener;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import me.steven.networkreward.NetworkReward;
import me.steven.networkreward.objectives.Type;
import me.steven.networkreward.playerfile.PlayerFile;

public class Walk implements Listener {
	NetworkReward plugin;
	public Walk(NetworkReward plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void playerMoveEvent(PlayerMoveEvent e){
	    Location to = e.getTo();
	    Location from = e.getFrom();
	    if (to.getBlockX() == from.getBlockX() && to.getBlockZ() == from.getBlockZ()) {
	        return; 
	    }
	    PlayerFile pf = plugin.getPlayerFileManager().getPlayerFile(e.getPlayer());
	    if(pf.getObjective().getType().equals(Type.WALK)){
	    	pf.incrementAmountDone();
	    }
	    
	}

}
