package me.steven.networkreward.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import me.steven.networkreward.NetworkReward;
import me.steven.networkreward.objectives.Type;
import me.steven.networkreward.playerfile.PlayerFile;

public class BlockBreak implements Listener {
	NetworkReward plugin;

	public BlockBreak(NetworkReward plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onBlockBreak(BlockBreakEvent e) {
		PlayerFile pf = plugin.getPlayerFileManager().getPlayerFile(e.getPlayer());

		if (pf.getObjective().getType() == Type.DESTROY) {
			if (e.getBlock().getType() == pf.getBlock()) {
				pf.incrementAmountDone();
			}
		}
	}

}
