package me.steven.networkreward.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

import me.steven.networkreward.NetworkReward;
import me.steven.networkreward.objectives.Type;
import me.steven.networkreward.playerfile.PlayerFile;

public class BlockPlace implements Listener {

	NetworkReward plugin;

	public BlockPlace(NetworkReward plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e) {
		PlayerFile pf = plugin.getPlayerFileManager().getPlayerFile(e.getPlayer());
		if (pf.getObjective().getType() == Type.BUILD) {
			if (e.getBlock().getType().equals(pf.getBlock())) {
				pf.incrementAmountDone();
			}
		}
	}

}
