package me.steven.networkreward.commands;

import java.util.UUID;

import me.steven.networkreward.playerfile.PlayerFile;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.steven.networkreward.NetworkReward;

public class CommandListener implements CommandExecutor {
	private NetworkReward plugin;

	public CommandListener(NetworkReward plugin) {
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String args[]) {

		if (cmd.getName().equalsIgnoreCase("networkreward")) {
			if (args.length > 0) {
				if (args[0].equalsIgnoreCase("give")) {
					if (!sender.hasPermission("NetworkReward.give")) {
						return false;
					}
					if (args.length < 3) {
						sender.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString(".notEnoughArguments").replace("{prefix}", plugin.getConfig().getString(".prefix"))));
					} else {
						@SuppressWarnings("deprecation")
						UUID uuid = Bukkit.getOfflinePlayer(args[1]).getUniqueId();
						int coins = Integer.parseInt(args[2]);
						if (coins != 0 && uuid != null) {
							plugin.getPlayerData().addCoins(uuid, coins);
							if(Bukkit.getOfflinePlayer(uuid).isOnline()) {
								Bukkit.getPlayer(uuid).sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString(".setCoins")
										.replace("{prefix}", plugin.getConfig().getString(".prefix"))
										.replace("{name}", args[1])
										.replace("{coins}", plugin.getPlayerData().getCoins(uuid) + "")));
							}
							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString(".setCoins")
									.replace("{prefix}", plugin.getConfig().getString(".prefix"))
									.replace("{name}", args[1])
									.replace("{coins}", plugin.getPlayerData().getCoins(uuid) + "")));
						} else {
							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString(".notValid")
									.replace("{prefix}", plugin.getConfig().getString(".prefix"))));
						}
					}
					return false;
				}
				if (sender instanceof Player) {
					if (args[0].equalsIgnoreCase("scoreboard")) {
						PlayerFile pf = plugin.getPlayerFileManager().getPlayerFile(((Player) sender).getPlayer());
						pf.setScoreboardEnabled(!pf.isScoreboardEnabled());
						sender.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString(".togglescoreboard")
								.replace("{prefix}", plugin.getConfig().getString(".prefix"))));
						return false;
					}
				}
			}
			for (String help : plugin.getConfig().getStringList(".help")) {
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', help));
			}
			return false;
		}
		if (!(sender instanceof Player)) {
			sender.sendMessage("Command sender must be a player!");
			return false;
		}
		Player p = ((Player) sender).getPlayer();
		if (cmd.getName().equalsIgnoreCase("shop")) {
			// open shop
			plugin.getShopManager().openShop(p);
			return false;
		}
		return false;
	}
}
