package me.steven.networkreward;

import me.steven.networkreward.commands.CommandListener;
import me.steven.networkreward.config.ConfigManager;
import me.steven.networkreward.listener.*;
import me.steven.networkreward.metrics.Metrics;
import me.steven.networkreward.mysql.MysqlConnection;
import me.steven.networkreward.mysql.PlayerData;
import me.steven.networkreward.objectives.ObjectiveManager;
import me.steven.networkreward.playerfile.PlayerFile;
import me.steven.networkreward.playerfile.PlayerFileManager;
import me.steven.networkreward.rewards.RewardManager;
import me.steven.networkreward.scoreboard.ScoreboardManager;
import me.steven.networkreward.shop.ShopItemManager;
import me.steven.networkreward.shop.ShopManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class NetworkReward extends JavaPlugin {
	private ConfigManager cm;
	private ScoreboardManager scm;
	private RewardManager rwm;
	private ObjectiveManager om;
	private MysqlConnection connect;
	private PlayerFileManager playerFileManager;
	private PlayerData playerData;
	private ShopItemManager shopItemManager;
	private ShopManager shopManager;
	@Override
	public void onEnable() {
		// load plugin listeners
		new Metrics(this);
		Bukkit.getPluginManager().registerEvents(new Join(this), this);
		Bukkit.getPluginManager().registerEvents(new Leave(this), this);
		Bukkit.getPluginManager().registerEvents(new Walk(this), this);
		Bukkit.getPluginManager().registerEvents(new Jump(this), this);
		Bukkit.getPluginManager().registerEvents(new BlockBreak(this), this);
		Bukkit.getPluginManager().registerEvents(new BlockPlace(this), this);
		Bukkit.getPluginManager().registerEvents(new Kill(this), this);
		Bukkit.getPluginManager().registerEvents(new InventoryClick(this), this);
		getCommand("networkreward").setExecutor(new CommandListener(this));
		getCommand("shop").setExecutor(new CommandListener(this));
		// initialize managers
		cm = new ConfigManager(this);
		cm.init();
		connect = new MysqlConnection(this);
		if (!connect.init()) {
			getLogger().severe("Can't connect to database! disabling plugin");
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}
		rwm = new RewardManager(this);
		rwm.init();
		om = new ObjectiveManager(this);
		om.init();
		playerData = new PlayerData(this);
		playerFileManager = new PlayerFileManager(this);
		playerFileManager.init();
		scm = new ScoreboardManager(this);
		scm.init();
		shopItemManager = new ShopItemManager(this);
		shopItemManager.init();
		shopManager = new ShopManager(this);
		Timer timer = new Timer(this);
		timer.scoreBoardClock(this.getConfig().getInt("scoreboardUpdate") * 20);
		timer.minuteTimer(60 * 20);

		for (Player p : Bukkit.getOnlinePlayers()) {
			PlayerFile pf = getPlayerFileManager().getPlayerFile(p);
			pf.setCoins(getPlayerData().getCoins(p.getUniqueId()));
		}
	}

	@Override
	public void onDisable() {
		// save all player files.
		for (Player p : Bukkit.getOnlinePlayers()) {
			getPlayerFileManager().getPlayerFile(p).save();
		}
	}

	public ShopManager getShopManager() {
		return shopManager;
	}

	public ShopItemManager getShopItemManager() {
		return shopItemManager;
	}

	public PlayerData getPlayerData() {
		return playerData;
	}

	public MysqlConnection getConnection() {
		return connect;
	}

	public PlayerFileManager getPlayerFileManager() {
		return playerFileManager;
	}

	public ScoreboardManager getScoreboardManager() {
		return scm;
	}

	public ConfigManager getConfigManager() {
		return cm;
	}

	public RewardManager getRewardManager() {
		return rwm;
	}

	public ObjectiveManager getObjectiveManager() {
		return om;
	}
}
