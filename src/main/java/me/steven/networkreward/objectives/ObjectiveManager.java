package me.steven.networkreward.objectives;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.EntityType;

import me.steven.networkreward.NetworkReward;
import me.steven.networkreward.rewards.Reward;

public class ObjectiveManager {

	private NetworkReward plugin;
	private List<Objective> objectiveList = new ArrayList<>();

	public ObjectiveManager(NetworkReward plugin) {
		this.plugin = plugin;
	}

	public void init() {
		objectiveList = loadObjectives();
		plugin.getLogger().log(Level.ALL, "loaded objectives");
		// load all objectives.
	}

	public Objective getRandomObjective() {
		Random randomGenerator = new Random();
		int random = randomGenerator.nextInt(objectiveList.size());
		return objectiveList.get(random);
	}

	private List<Objective> loadObjectives() {
		List<Objective> list = new ArrayList<>();
		FileConfiguration data = YamlConfiguration
				.loadConfiguration(new File(plugin.getDataFolder(), "objectives.yml"));
		for (String s : data.getKeys(false)) {
			Objective objective = new Objective(plugin, s);
			if (data.getString(s + ".description") != null) {
				objective.setDescription(data.getString(s + ".description"));
			}
			if (data.getString(s + ".reward") != null) {
				Reward reward = plugin.getRewardManager().getReward(data.getString(s + ".reward"));
				if (reward != null) {
					objective.setReward(reward);
				}
			}
			if (data.getString(s + ".type") != null) {
				Type type = Type.valueOf(data.getString(s + ".type"));
				if (type != null) {
					objective.setType(type);
				}
			}
			List<String> mobs = data.getStringList(s + ".mob");
			if (!mobs.isEmpty()) {
				for (String mob : mobs) {
					EntityType entity = EntityType.valueOf(mob);
					objective.addMob(entity);
					plugin.getLogger().info("Loaded mob: " + mob.toString());
				}
			}
			List<String> blocks = data.getStringList(s + ".block");
			if (!blocks.isEmpty()) {
				for (String block : blocks) {
					Material blockType = Material.valueOf(block);
					objective.addBlock(blockType);
					plugin.getLogger().info("Loaded block: " + block.toString());

				}
			}
			if (data.getInt(s + ".amount") != 0) {
				objective.setAmount(data.getInt(s + ".amount"));
			}

			list.add(objective);
			// Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&',
			// "&a Loaded objective: " + s));
			plugin.getLogger().info("loaded objective:" + s);
		}
		return list;
	}

	public Objective getObjective(String name) {
		return objectiveList.stream().filter(d -> d.getName().equals(name)).findFirst().orElse(null);
	}

}
