package me.steven.networkreward.objectives;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;

import me.steven.networkreward.NetworkReward;
import me.steven.networkreward.rewards.Reward;

public class Objective {
	// private NetworkReward plugin;
	private String name;
	private String description;
	private Reward reward;
	private Type type;
	private List<Material> blocks = new ArrayList<>();
	private List<EntityType> mobs = new ArrayList<>();
	private int amount;

	public Objective(NetworkReward plugin, String name) {
		// this.plugin = plugin;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	// public Material getBlock() {
	// return block;
	// }
	//
	// public void setBlock(Material block) {
	// this.block = block;
	// }
	public List<Material> getBlocks() {
		return blocks;
	}

	public void addBlock(Material block) {
		blocks.add(block);
	}

	public List<EntityType> getMobs() {
		return mobs;
	}

	public void addMob(EntityType mob) {
		mobs.add(mob);
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public Reward getReward() {
		return reward;
	}

	public void setReward(Reward reward) {
		this.reward = reward;
	}
}
