package me.steven.networkreward.config;

import java.io.File;

import me.steven.networkreward.NetworkReward;

public class ConfigManager {

	NetworkReward plugin;

	public ConfigManager(NetworkReward plugin) {
		this.plugin = plugin;
	}

	public void init() {
		// create default configs.
		loadConfig();
		loadObjectives();
		loadRewards();
		loadScoreboard();
		loadShopItems();
	}

	private void loadConfig() {
		try {
			if (!plugin.getDataFolder().exists()) {
				plugin.getDataFolder().mkdirs();
			}
			File file = new File(plugin.getDataFolder(), "config.yml");
			if (!file.exists()) {
				plugin.getLogger().info("Config.yml not found, creating!");
				plugin.saveDefaultConfig();
			} else {
				plugin.getLogger().info("Config.yml found, loading!");
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	private void loadObjectives() {
		try {
			if (!plugin.getDataFolder().exists()) {
				plugin.getDataFolder().mkdirs();
			}
			File file = new File(plugin.getDataFolder(), "objectives.yml");
			if (!file.exists()) {
				plugin.getLogger().info("objectives.yml not found, creating!");
				plugin.saveResource("objectives.yml", false);
			} else {
				plugin.getLogger().info("objectives.yml found, loading!");
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	private void loadRewards() {
		try {
			if (!plugin.getDataFolder().exists()) {
				plugin.getDataFolder().mkdirs();
			}
			File file = new File(plugin.getDataFolder(), "rewards.yml");
			if (!file.exists()) {
				plugin.getLogger().info("rewards.yml not found, creating!");
				plugin.saveResource("rewards.yml", false);
			} else {
				plugin.getLogger().info("rewards.yml found, loading!");
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	private void loadScoreboard() {
		try {
			if (!plugin.getDataFolder().exists()) {
				plugin.getDataFolder().mkdirs();
			}
			File file = new File(plugin.getDataFolder(), "scoreboard.yml");
			if (!file.exists()) {
				plugin.getLogger().info("scoreboard.yml not found, creating!");
				plugin.saveResource("scoreboard.yml", false);
			} else {
				plugin.getLogger().info("scoreboard.yml found, loading!");
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	private void loadShopItems() {
		try {
			if (!plugin.getDataFolder().exists()) {
				plugin.getDataFolder().mkdirs();
			}
			File file = new File(plugin.getDataFolder(), "shopitems.yml");
			if (!file.exists()) {
				plugin.getLogger().info("shopitems.yml not found, creating!");
				plugin.saveResource("shopitems.yml", false);
			} else {
				plugin.getLogger().info("shopitems.yml found, loading!");
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
	}
}
