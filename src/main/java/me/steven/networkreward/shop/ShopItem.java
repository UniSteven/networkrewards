package me.steven.networkreward.shop;

import java.util.List;

import org.bukkit.Material;

import me.steven.networkreward.NetworkReward;
import me.steven.networkreward.rewards.Reward;

public class ShopItem {
	private int cost;
	private Material icon;
	private Reward reward;
	private NetworkReward plugin;
	private List<String> description;
	private int id;
	private String name = "";
	private String requirement = "";
	public ShopItem(int cost, Material icon, Reward reward, NetworkReward plugin, List<String> desc, int id,
			String name) {
		super();
		this.cost = cost;
		this.icon = icon;
		this.reward = reward;
		this.plugin = plugin;
		this.description = desc;
		this.id = id;
		this.name = name;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public Material getIcon() {
		return icon;
	}

	public void setIcon(Material icon) {
		this.icon = icon;
	}

	public Reward getReward() {
		return reward;
	}

	public void setReward(Reward reward) {
		this.reward = reward;
	}

	public NetworkReward getPlugin() {
		return plugin;
	}

	public List<String> getDescription() {
		return description;
	}

	public void setDescription(List<String> description) {
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRequirement() {
		return requirement;
	}

	public void setRequirement(String requirement) {
		this.requirement = requirement;
	}

	public void setPlugin(NetworkReward plugin) {
		this.plugin = plugin;
	}

}
