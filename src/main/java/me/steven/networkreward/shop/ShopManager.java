package me.steven.networkreward.shop;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import me.steven.networkreward.NetworkReward;
import me.steven.networkreward.playerfile.PlayerFile;

public class ShopManager {
	private NetworkReward plugin;

	public ShopManager(NetworkReward plugin) {
		this.plugin = plugin;
	}
	
	
	public void openShop(Player p){
		int size = plugin.getShopItemManager().getShopItems().size();
		PlayerFile pf = plugin.getPlayerFileManager().getPlayerFile(p);
		int totalMenus = size / 36;
		if(totalMenus < 1){ totalMenus = 1;}else{totalMenus = totalMenus+1;}
		int menuNum = pf.getMenuNum();
		int counter = 0;
		List<ShopItem> shopItemList = new ArrayList<>();
		for(int i = 0; i < size; i++){
			if(counter <= (counter * menuNum) && counter >= (counter * (menuNum - 1)) ){
				ShopItem it = plugin.getShopItemManager().getShopItems().get(counter);
				shopItemList.add(it);
			}
			counter++;
		}
		Shop shop = new Shop(plugin, menuNum, plugin.getConfig().getString(".shopName"), shopItemList, 36);
		shop.openMenu(p);
	}
}
