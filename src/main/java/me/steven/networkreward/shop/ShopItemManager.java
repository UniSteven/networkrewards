package me.steven.networkreward.shop;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import me.steven.networkreward.NetworkReward;
import me.steven.networkreward.rewards.Reward;

public class ShopItemManager {
	private NetworkReward plugin;
	List<ShopItem> shopItemList = new ArrayList<>();

	public ShopItemManager(NetworkReward plugin) {
		this.plugin = plugin;
	}

	public void init() {
		// load all shopitems
		shopItemList = loadShopItems();
	}

	int shopItemID = 0;

	private List<ShopItem> loadShopItems() {
		List<ShopItem> items = new ArrayList<>();
		FileConfiguration data = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "shopitems.yml"));
		for (String s : data.getKeys(false)) {
			int cost = data.getInt(s + ".coins");
			Material icon = Material.valueOf(data.getString(s + ".icon"));
			Reward reward = plugin.getRewardManager().getReward(data.getString(s + ".reward"));
			List<String> desc = data.getStringList(s + ".description");
			String name = data.getString(s + ".name");
			ShopItem si = new ShopItem(cost, icon, reward, plugin, desc, shopItemID, name);
			if(data.getString(s + ".require") != null){
			si.setRequirement(data.getString(s + ".require"));
			}
			items.add(si);
			plugin.getLogger().info("Loaded shopItem: " + s + " ID: " + shopItemID);
			shopItemID++;
		}
		return items;
	}

	public List<ShopItem> getShopItems() {
		return shopItemList;
	}
}
