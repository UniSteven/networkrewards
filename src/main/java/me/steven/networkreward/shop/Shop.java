package me.steven.networkreward.shop;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.steven.networkreward.NetworkReward;
import me.steven.networkreward.playerfile.PlayerFile;

public class Shop {
	private NetworkReward plugin;
	private int menuNum;
	private String name;
	private List<ShopItem> items = new ArrayList<>();
	private int size;

	public Shop(NetworkReward plugin, int menuNum, String name, List<ShopItem> items, int size) {
		this.plugin = plugin;
		this.menuNum = menuNum;
		this.name = name;
		this.items = items;
		this.size = size;
	}

	int itemId = 0;

	public void openMenu(Player p) {
		Inventory menu = Bukkit.createInventory(null, size,
				ChatColor.translateAlternateColorCodes('&', name + menuNum));
		PlayerFile pf = plugin.getPlayerFileManager().getPlayerFile(p);
		for (ShopItem item : items) {
			ItemStack itemStack = new ItemStack(item.getIcon(), 1);
			ItemMeta itemMeta = itemStack.getItemMeta();
			itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', item.getName()));
			ArrayList<String> lore = new ArrayList<String>();
			for (String s : item.getDescription()) {
				lore.add(ChatColor.translateAlternateColorCodes('&', s));
			}
			if (pf.getCoins() - item.getCost() > 0) {
				lore.add(ChatColor.translateAlternateColorCodes('&', "&6Price: &2" + item.getCost()));
			} else {
				lore.add(ChatColor.translateAlternateColorCodes('&', "&6Price: &c" + item.getCost()));
			}
			itemMeta.setLore(lore);
			itemStack.setItemMeta(itemMeta);
			menu.setItem(itemId, itemStack);
			itemId++;
		}
		p.openInventory(menu);
	}

}
