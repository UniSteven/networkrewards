package me.steven.networkreward;

import me.steven.networkreward.mysql.PlayerData;
import me.steven.networkreward.playerfile.PlayerFile;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import me.steven.networkreward.rewards.Reward;

public class Timer {
	NetworkReward plugin;
	
	public Timer(NetworkReward plugin) {
		this.plugin = plugin;
	}

	public void scoreBoardClock(int time) {
		Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
			public void run() {
				if(plugin.getConfig().getBoolean(".scoreboard")) {
					for (Player p : Bukkit.getOnlinePlayers()) {
						PlayerFile pf = plugin.getPlayerFileManager().getPlayerFile(p);
						if(pf.isScoreboardEnabled()) {
							plugin.getScoreboardManager().updatePlayer(p);
						}else{
							plugin.getScoreboardManager().removeScoreBoard(p);
						}
					}
				}
				
			}
		}, 0, time);

	}
	
	public void minuteTimer(int time) {
		Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
			public void run() {
				for(Player p : Bukkit.getOnlinePlayers()){
					PlayerFile pf = plugin.getPlayerFileManager().getPlayerFile(p);
					plugin.getPlayerData().incrementTimePlayer(p.getUniqueId());
					pf.setPlayTime(pf.getPlayTime() +1);
					for(Reward reward : plugin.getRewardManager().getRewardList()){
						int timePlayed = plugin.getPlayerData().getTime(p.getUniqueId());
						if(reward.getTime() == timePlayed){
							// give reward. this method might not be that efficient. need to learn better ways :P
							int coins = reward.getCoins();
							if(coins != 0){
								plugin.getPlayerData().addCoins(p.getUniqueId(), coins);
							}
							String message = ChatColor.translateAlternateColorCodes('&', reward.getMessage().replace("{prefix}", plugin.getConfig().getString(".prefix")));
							if(message != null){
							p.sendMessage(message);
							}
							if(reward.getCommand() != null){
								Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), reward.getCommand().replace("{player}", p.getName())); // execute command when reward is set to have a command.
							}

							pf.addRequirement(reward.getName().toLowerCase());
						}
					}
					
				}
				
			}
		}, 0, time);

	}
}
