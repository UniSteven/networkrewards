package me.steven.networkreward.rewards;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import me.steven.networkreward.NetworkReward;

public class RewardManager {
	private NetworkReward plugin;
	private List<Reward> rewardsList = new ArrayList<>();
	public RewardManager(NetworkReward plugin) {
		this.plugin = plugin;
	}
	
	public void init(){
		rewardsList = loadRewards();
		// load all rewards and put it in a list
	}
	public List<Reward> getRewardList(){
		return rewardsList;
	}
	private List<Reward> loadRewards(){
		List<Reward> list = new ArrayList<>();
		FileConfiguration data = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "rewards.yml"));
		for(String s : data.getKeys(false)){
			Reward reward = new Reward(plugin, s);
			if(data.getString(s + ".command") != null){
				reward.setCommand(data.getString(s + ".command"));
			}
			if(data.getInt(s + ".time") != 0){
				reward.setTime(data.getInt(s + ".time"));
			}
			if(data.getString(s + ".message") != null){
				reward.setMessage(data.getString(s + ".message"));
			}
			if(data.getInt(s + ".coins") != 0){
				reward.setCoins(data.getInt(s + ".coins"));
			}
			
			list.add(reward);
			plugin.getLogger().info("loaded reward:" + s);
		}
		return list;
	}
	
	public Reward getReward(String name){
		return rewardsList.stream().filter(d -> d.getName().equals(name)).findFirst().orElse(null);
	}
}
