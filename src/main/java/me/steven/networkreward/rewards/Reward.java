package me.steven.networkreward.rewards;

import me.steven.networkreward.NetworkReward;

public class Reward {
	private String name;
	private int time;
	private String command;
	private String message;
	private int coins;
	private NetworkReward plugin;
	public Reward(NetworkReward plugin, String name){
		this.plugin = plugin;
		this.name = name;
	}
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getCoins() {
		return coins;
	}
	public void setCoins(int coins) {
		this.coins = coins;
	}
	public NetworkReward getPlugin() {
		return plugin;
	}
	public void setPlugin(NetworkReward plugin) {
		this.plugin = plugin;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
