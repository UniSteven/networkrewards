package me.steven.networkreward.scoreboard;

import org.bukkit.entity.Player;

import me.steven.networkreward.NetworkReward;
import me.steven.networkreward.objectives.Type;
import me.steven.networkreward.playerfile.PlayerFile;

public class Lines {

	private int lineNumber;
	private Player p;
	private NetworkReward plugin;
	private String lineText;

	Lines(int lineNumber, String lineText, Player p, NetworkReward plugin) {
		this.lineNumber = lineNumber;
		this.lineText = lineText;
		this.plugin = plugin;
		this.p = p;
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}

	public String getLineText() {
		// replace all parameters with values
		PlayerFile pf = plugin.getPlayerFileManager().getPlayerFile(p);
		if (lineText.contains("{block}") && !((pf.getObjective().getType().equals(Type.BUILD)
				|| pf.getObjective().getType().equals(Type.DESTROY)))) {
			return null;
		}
		if (lineText.contains("{mob}") && !((pf.getObjective().getType().equals(Type.KILL)))) {
			return null;
		}
		String finalLineText = lineText.replace("{objective}", pf.getObjective().getName())
				.replace("{done}", pf.getAmountDone() + "").replace("{target}", pf.getObjective().getAmount() + "")
				.replace("{coins}", pf.getCoins() + "").replace("{block}", pf.getBlock().toString())
				.replace("{mob}", pf.getMob().toString())
				.replace("{playtime}", pf.getConvertedPlayTime() + "");
		return finalLineText;
	}

	public void setLineText(String lineText) {
		this.lineText = lineText;
	}
}
