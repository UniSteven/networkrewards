package me.steven.networkreward.scoreboard;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;

import me.steven.networkreward.NetworkReward;

public class Scoreboard {

	private Player p;
	private String name;
	private List<Lines> lines = new ArrayList<Lines>();
	private NetworkReward plugin;
	private int linenumber = 0;

	public Scoreboard(Player p, String name, NetworkReward plugin) {
		this.p = p;
		this.name = name;
		this.plugin = plugin;
	}

	public Player getP() {
		return p;
	}

	public void setP(Player p) {
		this.p = p;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Lines> getLines() {
		return lines;
	}

	public void setLines(List<Lines> lines) {
		this.lines = lines;
	}

	public void addline(String line) {
		Lines ls = new Lines(linenumber, line, p, plugin);
		lines.add(ls);
		linenumber--;
	}

	public void updateScoreboard() {
		org.bukkit.scoreboard.Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
		Objective obj = board.registerNewObjective("sb", "dummy");
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		obj.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
		// obj.getScore("test").setScore(0); // test score.
		Iterator<Lines> itr = lines.iterator();
		while (itr.hasNext()) {
			Lines l = itr.next();
			if (l.getLineText() != null) {
				obj.getScore(ChatColor.translateAlternateColorCodes('&', l.getLineText())).setScore(l.getLineNumber());
			}
		}
		p.setScoreboard(board);

	}
}
