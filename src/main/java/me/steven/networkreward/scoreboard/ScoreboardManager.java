package me.steven.networkreward.scoreboard;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import me.steven.networkreward.NetworkReward;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;

public class ScoreboardManager {
	NetworkReward plugin;
	List<Scoreboard> list = new ArrayList<>();
	String serverName;
	public ScoreboardManager(NetworkReward plugin) {
		this.plugin = plugin;
	}
	
	public void init(){
		serverName = plugin.getConfig().getString("networkName");
		list = loadScoreboards(); // load scoreboards for players online.
	}
	
	private List<Scoreboard> loadScoreboards(){
		for(Player p : Bukkit.getOnlinePlayers()){
			
			Scoreboard b = new Scoreboard(p, serverName, plugin);
			b.setLines(loadLines(p));
			b.updateScoreboard();
			list.add(b);
		}
		return list;
	}
	
	public void updatePlayer(Player p){
		if(!(getScoreboard(p) == null)){
		getScoreboard(p).updateScoreboard(); // run this in a loop defined in the config
		}else{
			loadPlayer(p);
		}
	}
	public void removeScoreBoard(Player p){
		org.bukkit.scoreboard.Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
		Objective obj = board.registerNewObjective("sb", "dummy");
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		p.setScoreboard(board);
	}
	private List<Lines> loadLines(Player p){
		List<Lines> lines = new ArrayList<>();
		FileConfiguration data = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "scoreboard.yml"));
		int i= 0;
		for(String s : data.getStringList("lines")){
			i--;
			Lines l = new Lines(i, s, p, plugin);
			lines.add(l);
		}
		return lines;
	}
	public void loadPlayer(Player p){
		if(getScoreboard(p) == null){
			Scoreboard b = new Scoreboard(p, serverName, plugin);
			list.add(b);
			b.setLines(loadLines(p));
			//b.updateScoreboard();
		}
	}
	public Scoreboard getScoreboard(Player p) {
		return list.stream().filter(d -> d.getP() == p).findFirst().orElse(null);
	}
	
	
	
}
